<?php

use Illuminate\Support\Facades\Route;

Route::group(
    [
    'prefix' => '{locale?}',
    'middleware' => 'setlocale'], function () {
        Route::get('/', 'ProductController@index');
        Route::get('/products', 'ProductController@card');
    }
);

Route::get('/products/update', 'ProductController@update');
