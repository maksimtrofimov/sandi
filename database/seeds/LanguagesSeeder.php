<?php

use Illuminate\Database\Seeder;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert(
            [
                'name' => 'Ukraine',
                'iso' => 'uk',
                'default' => 1,
                'activated' => 1,
            ]
        );

        DB::table('languages')->insert(
            [
                'name' => 'Russian',
                'iso' => 'ru',
                'activated' => 1,
            ]
        );
    }
}
