<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [
            'sku' => '232dsfsdf',
            'name_ru' => 'product 1 rus',
            'description_ru' => 'description 1 rus',
            'name_uk' => 'product 1 uk',
            'description_uk' => 'description 1 uk',
            'price' => 100,
            ]
        );

        DB::table('products')->insert(
            [
            'sku' => '234324dssfsdf',
            'name_ru' => 'product 2 rus',
            'description_ru' => 'description 2 rus',
            'name_uk' => 'product 2 uk',
            'description_uk' => 'description 2 uk',
            'price' => 200,
            ]
        );

        DB::table('products')->insert(
            [
            'sku' => '21124dssfsdf',
            'name_ru' => 'product 3 rus',
            'description_ru' => 'description 3 rus',
            'name_uk' => 'product 3 uk',
            'description_uk' => 'description 3 uk',
            'price' => 300,
            ]
        );
    }
}
