<?php

use Illuminate\Database\Seeder;

class CharacteristicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('characteristics')->insert(
            [
            'product_id' => 1,
            'name_ru' => 'product 1 rus',
            'value_ru' => 'value 1 rus',
            'name_uk' => 'product 1 uk',
            'value_uk' => 'value 1 uk',
            ]
        );

        DB::table('characteristics')->insert(
            [
            'product_id' => 2,
            'name_ru' => 'product 2 rus',
            'value_ru' => 'value 2 rus',
            'name_uk' => 'product 2 uk',
            'value_uk' => 'value 2 uk',
            ]
        );

        DB::table('characteristics')->insert(
            [
            'product_id' => 3,
            'name_ru' => 'product 3 rus',
            'value_ru' => 'value 3 rus',
            'name_uk' => 'product 3 uk',
            'value_uk' => 'value 3 uk',
            ]
        );

        DB::table('characteristics')->insert(
            [
            'product_id' => 3,
            'name_ru' => 'product 3.2 rus',
            'value_ru' => 'value 3.2 rus',
            'name_uk' => 'product 3.2 uk',
            'value_uk' => 'value 3.2 uk',
            ]
        );
    }
}
