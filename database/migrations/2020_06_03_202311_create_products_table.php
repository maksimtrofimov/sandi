<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'products', function (Blueprint $table) {
                $table->id();
                $table->string('sku');
                $table->string('name_ru');
                $table->text('description_ru')->nullable();
                $table->string('name_uk');
                $table->text('description_uk')->nullable();
                $table->unsignedInteger('price');
                $table->unique('sku');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
