<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharacteristicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'characteristics', function (Blueprint $table) {
                $table->id();
                $table->foreignId('product_id')
                    ->references('id')
                    ->on('products')
                    ->onDelete('cascade');
                $table->string('name_ru')->nullable();;
                $table->text('value_ru')->nullable();;
                $table->string('name_uk')->nullable();;
                $table->text('value_uk')->nullable();;
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'characteristics', function (Blueprint $table) {
                $table->dropForeign(['product_id']);
            }
        );

        Schema::dropIfExists('characteristics');
    }
}
