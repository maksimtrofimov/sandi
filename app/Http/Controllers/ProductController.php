<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $service;

    function __construct()
    {
        $this->service = new ProductService();
    }

    public function index()
    {
        return view(
            'products.list', [
            'products' => $this->service->all()
            ]
        );
    }

    public function card(Request $request)
    {
        return view(
            'products.card', [
            'product' => $this->service->one($request->input('id'))
            ]
        );
    }

    public function update()
    {
        $this->service->update();
        return redirect('/');
    }
}
