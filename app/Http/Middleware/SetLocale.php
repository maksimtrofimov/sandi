<?php

namespace App\Http\Middleware;

use Closure;

use App\Services\LanguageService;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        (new \App\Services\LanguageService())->setlocale();
        return $next($request);
    }
}
