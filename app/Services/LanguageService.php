<?php

namespace App\Services;

use App\Models\Language;

class LanguageService
{
    protected $languages;
    protected $iso;
    protected $default;

    protected function all()
    {
        return Language::activated()->get();
    }

    protected function languages()
    {
        foreach ($this->all() as $lang) {
            $this->languages[] = $lang->iso;
            $this->iso[$lang->iso] = $lang->name;
        }
        $this->default();
    }

    protected function default()
    {
        $this->default = $this->languages[0];
    }

    public function setlocale()
    {
        $iso = request()->segment(1);
        if (!($this->isSetLangInSession($iso) && $this->matchIso($iso))) {
            $this->languages();
            $iso = $this->isSetLanguage($iso) ? $iso : $this->default;
            $this->setSession($iso);
        }
        app()->setLocale($iso);
    }

    protected function isSetLangInSession($iso)
    {
        return request()->session()->has('lang') ? true : false;
    }

    protected function matchIso($iso)
    {
        return request()->session()->get('lang') == $iso;
    }

    protected function setSession($iso)
    {
        request()->session()->put('lang', $iso);
    }

    protected function isSetLanguage($code)
    {
        return isset($this->iso[$code]) ? true : false;
    }

}
