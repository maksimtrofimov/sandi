<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class ProductService
{
    protected $uri = 'https://b2b-sandi.com.ua/api/products';

    public function all()
    {
        return Product::translations()->get();
    }

    public function one($id)
    {
        return Product::translations()->get()->find($id);
    }

    public function update()
    {
        try {
            $products = $this->getProducts();
            DB::beginTransaction();
            $this->delete();
            foreach ($products as $product) {
                $product_id = $this->insertProduct($product);
                foreach ($product['characteristics'] as $characteristics) {
                    $this->insertCharacteristics($product_id, $characteristics);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    protected function getProducts()
    {
        return json_decode(Http::get($this->uri), JSON_UNESCAPED_UNICODE);
    }

    protected function insertProduct($product)
    {
        return DB::table('products')->insertGetId(
            [
                'sku'             => $product['sku'],
                'price'           => $product['price'],
                'name_ru'         => $product['name_ru'],
                'description_ru'  => $product['description_ru'],
                'name_uk'         => $product['name_uk'],
                'description_uk'  => $product['description_uk'],
            ]
        );
    }

    protected function insertCharacteristics($product_id, $characteristics)
    {
        DB::table('characteristics')->insert(
            [
                'product_id' => $product_id,
                'name_ru'    => $characteristics['name_ru'],
                'value_ru'   => $characteristics['value_ru'],
                'name_uk'    => $characteristics['name_uk'],
                'value_uk'   => $characteristics['value_uk'],
            ]
        );
    }

    protected function delete()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \App\Models\Characteristic::truncate();
        Product::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
