<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Product extends Model
{
    protected $locale;

    public function __construct()
    {
        $this->locale = App::getLocale();
    }

    public function characteristics()
    {
        return $this->hasMany(Characteristic::class)->select(['id', "name_{$this->locale} as name", "value_{$this->locale} as value"]);
    }

    public function scopeTranslations($query)
    {
        $query->select(['id', 'sku', "name_{$this->locale} as name", "description_{$this->locale} as description", 'price']);
    }
}
