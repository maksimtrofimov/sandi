<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function scopeActivated($query)
    {
        $query->where(['activated' => 1])->orderBy('default', 'desc');
    }
}
