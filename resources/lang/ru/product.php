<?php

return [
    'id'          => 'Ид',
    'sku'         => 'Ид товара',
    'price'       => 'Цена',
    'name'        => 'Названия',
    'description' => 'Описание',
    'more'        => 'Подробнее',
    'product'     => 'Продукт',
];
