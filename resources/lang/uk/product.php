<?php

return [
    'id'              => 'Ід',
    'sku'             => 'Ід товару',
    'price'           => 'Ціна',
    'name'            => 'Назва',
    'description'     => 'Опис',
    'more'            => 'Докладніше',
    'product'         => 'Продукт',
];
