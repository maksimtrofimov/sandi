@extends('layouts.app')
@section('content')

    <table class="table">
        <caption style="caption-side: top;">@lang('product.product')</caption>
        <tr>
            <td>@lang('product.id')</td>
            <td>{{ $product->id }}</td>
        </tr>
        <tr>
            <td>@lang('product.sku')</td>
            <td>{{ $product->sku }}</td>
        </tr>
        <tr>
            <td>@lang('product.price')</td
            ><td>{{ $product->price/100 }}</td>
        </tr>
        <tr>
            <td>@lang('product.name')</td>
            <td>{{ $product->name }}</td>
        </tr>
        <tr>
            <td>@lang('product.description')</td>
            <td>{{ $product->description }}</td>
        </tr>
    </table>

    <table class="table">
        <caption style="caption-side: top;">@lang('characteristic.characteristic')</caption>
        <thead>
            <tr>
                <th>@lang('characteristic.id')</th>
                <th>@lang('characteristic.name')</th>
                <th>@lang('characteristic.value')</th>
            </tr>
        </thead>
        @foreach($product->characteristics as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->value }}</td>
            </tr>
        @endforeach
    </table>
@endsection
