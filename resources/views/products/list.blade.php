@extends('layouts.app')
@section('content')
<table class="table">
    <tr>
        <th>@lang('product.id')</th>
        <th>@lang('product.sku')</th>
        <th>@lang('product.price')</th>
        <th>@lang('product.name')</th>
        <th>@lang('product.description')</th>
    </tr>
    @isset($products)
        @foreach($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->sku }}</td>
                <td>{{ $product->price/100 }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->description }}</td>
                <td><a href="{{ app()->getLocale() }}/products/?id={{ $product->id }}">@lang('product.more')</a></td>
            </tr>
        @endforeach
    @endisset
</table>
@endsection
